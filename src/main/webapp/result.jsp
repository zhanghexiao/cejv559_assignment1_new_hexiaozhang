<%-- 
    Document   : index.jsp
    Created on : 11-Feb-2019, 9:11:54 PM
    Author     : Hexiao Zhang
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>Self-Service Bank Calculation</title>
        <link rel="stylesheet" href="styles/main.css" type="text/css"/>
    </head>

    <body>
        <h1>Thanks for using the self-service bank calculation</h1>

        <p>Here is the information that you entered:</p>

        <label>Amount:</label>
        <span>${user.outputAmount}</span><br>
        <label>Time:</label>
        <span>${user.time}</span><br>
        <label>Rate:</label>
        <span>${user.rate}</span><br>
        <label>Result:</label>
        <span>${user.result}</span><br>
        
        <p>To calculate again, click on the Return button shown below.</p>

        <form action="index.jsp" method="post">
        <input type="submit" value="Return">
        </form>
    </body>




</html>