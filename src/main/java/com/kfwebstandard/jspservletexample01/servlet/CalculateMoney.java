package com.kfwebstandard.jspservletexample01.servlet;

import java.io.IOException;
import javax.servlet.RequestDispatcher;
import javax.servlet.ServletContext;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import com.kfwebstandard.jspservletexample01.model.User;
import com.kfwebstandard.jspservletexample01.persistence.UserIO;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 *
 * @author Ken
 */
@WebServlet(name = "CalculateMoney", urlPatterns = {"/CalculateMoney"})
public class CalculateMoney extends HttpServlet {

    private final static Logger LOG = LoggerFactory.getLogger(CalculateMoney.class);       
    
    /**
     * Handles the HTTP <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {

        String url = "/result.jsp";
        // get parameters from the request

        String time_string = request.getParameter("time");
        double time = (Double.parseDouble(time_string));
 
        String rate_string = request.getParameter("rate");
        double rate = Double.parseDouble(rate_string);
        
        String amount_string = request.getParameter("amount");
        double amount = Double.parseDouble(amount_string);
        
        String type_string = request.getParameter("type");
        int type = Integer.parseInt(type_string);

        String record = request.getParameter("save");

        // store data in User object
        User user = new User(time,type,amount,rate,record);
        user.performcalculation();
        
        // Read the context param from web.xml
        
        
        if (user.getRecord().equals("yes"))
         { 
        ServletContext context = getServletContext();
        String fileName = context.getInitParameter("CutomerInformation");        

        // write the User object to a file
        
        UserIO userIO = new UserIO();
        userIO.addRecord(user, fileName);
         }
         
        request.setAttribute("user", user);
        //session.setAttribute("user", user);
        // forward request and response to JSP page
        RequestDispatcher dispatcher
                = getServletContext().getRequestDispatcher(url);
        dispatcher.forward(request, response);
        
    }
}
